FROM python:3-stretch
ARG uid
ARG gid
ARG app_root_dir

WORKDIR /opt
RUN apt-get update \
    && apt-get --yes install openjdk-8-jdk-headless \
    && wget -q -O - https://archive.apache.org/dist/spark/spark-2.4.3/spark-2.4.3-bin-without-hadoop.tgz \
    | tar xz \
    && ln -sf /opt/spark-2.4.3-bin-without-hadoop /opt/spark \
    && echo 'export SPARK_DIST_CLASSPATH=$(hadoop classpath)' > /opt/spark/conf/spark-env.sh \
    && chmod 755 /opt/spark/conf/spark-env.sh \
    && groupadd -g ${gid} appgroup \
    && useradd -d ${app_root_dir} -m -u ${uid} -g ${gid} appuser

WORKDIR ${app_root_dir}
COPY docker-build/python/requirements-spark.txt .
RUN chown ${uid}:${gid} requirements-spark.txt && pip install --no-cache-dir -r requirements-spark.txt
COPY docker-build/python/requirements-other.txt .
RUN chown ${uid}:${gid} requirements-other.txt && pip install --no-cache-dir -r requirements-other.txt

USER appuser
RUN mkdir -p ${app_root_dir}/bin ${app_root_dir}/conf ${app_root_dir}/data ${app_root_dir}/lib

VOLUME ${app_root_dir}/bin
VOLUME ${app_root_dir}/conf
VOLUME ${app_root_dir}/data
VOLUME ${app_root_dir}/lib

CMD python bin/main.py