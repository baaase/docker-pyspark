# Project information

This project delivers data integration by using python code to interact with Apache Spark, using a docker container.

Please read the [deployment](#deployment) sections below for more information.

## Runtime information

The container will attempt to run the script named **main.py** in the **src** host folder.

Be aware that the host project folders are by default mounted into the container, with the following logic:
- **src**: mounted as *${docker_image_app_dir}/bin*
- **conf**: mounted as *${docker_image_app_dir}/conf*
- **data**: mounted as *${docker_image_app_dir}/data*
- **lib**: mounted as *${docker_image_app_dir}/lib*

## Forking/Cloning this project

When creating a new project based on this one, make sure to keep the `bin/*.sh` files executable. Procedure:

1. Import all files except the `bin/*.sh` ones into your git repository;
1. Run: `git add bin/*`;
1. For each file in the bin directory, run: `git update-index --chmod=+x bin/<filename>`;
1. Commit with `git commit -m"<Description example: Adding executable files>"`;
1. Push the commit into the repository using your preferred tool.

## Deployment

### Pre-requisites

This project requires linux with the following packages:
- git
- docker

Make sure to select the correct git branch prior to start.

### Configure

1. Run the script `bin/configure.sh` as the regular user that owns the code and the data. It will generate/update the required folders and configuration files;
1. Update the **conf/env** file with your own configurations.

### Install

**Required**: Complete the [configure](#configure) section.
**Optional**: If not the first time, complete the [uninstall](#uninstall) section.

1. Run the script `bin/install.sh` as root.

### Run

**Required**: Complete the [install](#install) section.

If running, scheduling or testing the final code (for the selected branch):
1. Run the script `bin/run.sh` as root.
If running, scheduling or testing a particular script:
1. Run the script `bin/run.sh <process name> <python script relative to /src> [<arg 1> <arg 2> ... <arg N>]` as root.

If debugging the generated image:
1. Run the script `bin/run-shell.sh` as root.

### Uninstall

**Required**: Complete the [configure](#configure) section.

1. Run the script `bin/uninstall.sh` as root.
