from pyspark import SparkContext, SQLContext, SparkConf
from pyspark.sql import SparkSession
from pyspark.sql import Row
import base64
import os
from dotenv import load_dotenv
import jaydebeapi
import time

########## Initialize ##########
# Record script's start for time elapsed calculation at the end
start_time = time.time()

load_dotenv('/app/conf/env')

sample_connection_properties = { \
    'driver': os.getenv('jdbc_classname_db2'), \
    'user': os.getenv('jdbc_conn_sample_user'), \
    'password': base64.b64decode(os.getenv('jdbc_conn_sample_password')).decode('utf-8'), \
    'sslTrustStoreLocation': os.getenv('jdbc_truststore_file') \
    }

spark = SparkSession.builder \
    .master(os.getenv('spark_master')) \
    .appName(os.getenv('project_name')) \
    .config('spark.jars', os.getenv('spark_jars')) \
    .config('spark.scheduler.mode', os.getenv('spark_scheduler')) \
    .config('spark.scheduler.allocation.file', os.getenv('spark_scheduler_file')) \
    .getOrCreate()

########## Extract ##########
source_query = '''(
select *
  from TEST.SOURCE
) t'''
target_table = 'TEST.TARGET'

DF_Expected = spark.read.jdbc(os.getenv('jdbc_conn_sample_url'), source_query, properties = sample_connection_properties)
DF_Current = spark.read.jdbc(os.getenv('jdbc_conn_sample_url'), target_table, properties = sample_connection_properties)

########## Transform ##########
DF_Inserts = DF_Expected.join(DF_Current, 'KEY', 'left_anti')
DF_Deletes = DF_Current.join(DF_Expected, 'KEY', 'left_anti')
DF_Both = DF_Expected.join(DF_Current, 'KEY', 'left_semi')
DF_Copies = DF_Both.join(DF_Current, DF_Both.columns, 'left_semi')
DF_Updates = DF_Both.subtract(DF_Copies)

########## Load ##########
def insert(rows):
    sample_connection = jaydebeapi.connect(sample_connection_properties['driver'], os.getenv('jdbc_conn_sample_url'), sample_connection_properties, os.getenv('spark_jars').split(','),)
    sample_cursor = sample_connection.cursor()
    sql = 'insert into {0} (KEY, DATA) values (?, ?)'.format(target_table)
    batch_size = int(os.getenv('jdbc_batch_size'))
    operations = 0
    parameters_list = []
    for row in rows:
        operations += 1
        parameters_list.append([row['KEY'], row['DATA']])
        if (operations % batch_size) == 0:
            sample_cursor.executemany(sql, parameters_list)
            del parameters_list[:]
    if len(parameters_list) > 0:
        sample_cursor.executemany(sql, parameters_list)
    sample_cursor.close()

def update(rows):
    sample_connection = jaydebeapi.connect(sample_connection_properties['driver'], os.getenv('jdbc_conn_sample_url'), sample_connection_properties, os.getenv('spark_jars').split(','),)
    sample_cursor = sample_connection.cursor()
    sql = 'update {0!s} set DATA = ? where KEY = ?'.format(target_table)
    batch_size = int(os.getenv('jdbc_batch_size'))
    operations = 0
    parameters_list = []
    for row in rows:
        operations += 1
        parameters_list.append([row['DATA'], row['KEY']])
        if (operations % batch_size) == 0:
            sample_cursor.executemany(sql, parameters_list)
            del parameters_list[:]
    if len(parameters_list) > 0:
        sample_cursor.executemany(sql, parameters_list)
    sample_cursor.close()

def delete(rows):
    sample_connection = jaydebeapi.connect(sample_connection_properties['driver'], os.getenv('jdbc_conn_sample_url'), sample_connection_properties, os.getenv('spark_jars').split(','),)
    sample_cursor = sample_connection.cursor()
    sql = 'delete from {0} where KEY = ?'.format(target_table)
    batch_size = int(os.getenv('jdbc_batch_size'))
    operations = 0
    parameters_list = []
    for row in rows:
        operations += 1
        parameters_list.append([row['KEY']])
        if (operations % batch_size) == 0:
            sample_cursor.executemany(sql, parameters_list)
            del parameters_list[:]
    if len(parameters_list) > 0:
        sample_cursor.executemany(sql, parameters_list)
    sample_cursor.close()

DF_Deletes.foreachPartition(delete)
DF_Updates.foreachPartition(update)
DF_Inserts.foreachPartition(insert)

inserts = DF_Inserts.count()
updates = DF_Updates.count()
deletes = DF_Deletes.count()

########## Cleanup ##########
spark.stop()

print('Processing time: {0!s}; Inserted: {1!s}; Updated: {2!s}; Deleted: {3!s}.'.format(time.strftime('%H:%M:%S', time.gmtime(time.time() - start_time)), inserts, updates, deletes))
