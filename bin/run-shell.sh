#!/bin/bash

##### Root user check
if [ $EUID -ne 0 ]; then
  1>&2 echo "This script must be run as root" 
  exit 1
fi

##### Configure the runtime environment
[ "${project_dir}" = "" ] && project_dir="$(cd $(dirname $0)/.. && pwd)"

if [ ! -f "${project_dir}/conf/env" ]; then
  1>&2 echo "Could not find the configuration file; exiting."
  exit 1
fi

source "${project_dir}/conf/env"

##### Run the project
docker run -it --rm --name "${docker_container_tag}" --network=host -v "${project_dir}/src":"${docker_image_app_dir}/bin":ro -v "${project_dir}/conf":"${docker_image_app_dir}/conf" -v "${project_dir}/data":"${docker_image_app_dir}/data" -v "${project_dir}/lib":"${docker_image_app_dir}/lib":ro "${docker_image_tag}" /bin/bash
