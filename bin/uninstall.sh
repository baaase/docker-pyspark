#!/bin/bash

##### Root user check
if [ $EUID -ne 0 ]; then
  1>&2 echo "This script must be run as root"
  exit 1
fi

##### Configure the runtime environment
[ "${project_dir}" = "" ] && project_dir="$(cd $(dirname $0)/.. && pwd)"

if [ ! -f "${project_dir}/conf/env" ]; then
  1>&2 echo "Could not find the configuration file; exiting."
  exit 1
fi

source "${project_dir}/conf/env"

##### Uninstall the project
docker image rm "${docker_image_tag}"
