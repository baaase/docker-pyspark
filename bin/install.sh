#!/bin/bash

##### Root user check
if [ $EUID -ne 0 ]; then
  1>&2 echo "This script must be run as root"
  exit 1
fi

##### Configure the runtime environment
[ "${project_dir}" = "" ] && project_dir="$(cd $(dirname $0)/.. && pwd)"

if [ ! -f "${project_dir}/conf/env" ]; then
  1>&2 echo "Could not find the configuration file; exiting."
  exit 1
fi

source "${project_dir}/conf/env"

##### Install the project
docker build --tag="${docker_image_tag}" --build-arg uid="${docker_image_app_uid}" --build-arg gid="${docker_image_app_gid}" --build-arg app_root_dir="${docker_image_app_dir}" "${project_dir}"
