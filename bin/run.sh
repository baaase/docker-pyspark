#!/bin/bash

##### Root user check
if [ $EUID -ne 0 ]; then
  1>&2 echo "This script must be run as root" 
  exit 1
fi

##### Configure the runtime environment
[ "${project_dir}" = "" ] && project_dir="$(cd $(dirname $0)/.. && pwd)"

if [ ! -f "${project_dir}/conf/env" ]; then
  1>&2 echo "Could not find the configuration file; exiting."
  exit 1
fi

source "${project_dir}/conf/env"

##### Verify arguments
if [ $# -eq 1 ]; then
  1>&2 echo "Usage: \"${0}\""
  1>&2 echo "   or: \"${0}\" <process name> <python script relative to src/> [<arg 1> <arg 2> ... <arg N>]"
fi

##### Run the project
if [ $# -eq 0 ]; then
  docker run --rm --name "${docker_container_tag}" --network=host -v "${project_dir}/src":"${docker_image_app_dir}/bin":ro -v "${project_dir}/conf":"${docker_image_app_dir}/conf" -v "${project_dir}/data":"${docker_image_app_dir}/data" -v "${project_dir}/lib":"${docker_image_app_dir}/lib":ro "${docker_image_tag}"
else
  docker run --rm --name "${docker_container_tag}-${1}" --network=host -v "${project_dir}/src":"${docker_image_app_dir}/bin":ro -v "${project_dir}/conf":"${docker_image_app_dir}/conf" -v "${project_dir}/data":"${docker_image_app_dir}/data" -v "${project_dir}/lib":"${docker_image_app_dir}/lib":ro "${docker_image_tag}" python bin/"${@:2}"
fi
