#!/bin/bash

##### Non-root user check
if [ $EUID -eq 0 ]; then
  1>&2 echo "This script cannot be run as root"
  exit 1
fi

[ "${project_dir}" = "" ] && project_dir="$(cd $(dirname $0)/.. && pwd)"

##### Configure the directories
[ ! -d "${project_dir}/conf/" ] && mkdir -p "${project_dir}/conf/"
[ ! -d "${project_dir}/data/" ] && mkdir -p "${project_dir}/data/"

##### Configure the env file
if [ ! -f "${project_dir}/conf/env" ]; then
  [ ! -f "${project_dir}/conf/env" ] && touch "${project_dir}/conf/env"
fi

configure_env()
{
  if [ "${1}" = "" ]; then
    1>&2 "configure_env: Missing operand. Exiting."
    exit 2
  fi
  grep -qe "^${1}=" "${project_dir}/conf/env"
  if [ $? -ne 0 ]; then
    export "${1}"="${2}"
    if [ "${2}" = "" ] ; then
      echo "${1}=" >> "${project_dir}/conf/env"
    else
      echo "${1}=\"${2}\"" >> "${project_dir}/conf/env"
    fi
  fi
}

### Load current values
source "${project_dir}/conf/env"

### Create missing variables
configure_env "project_dir" "${project_dir}"
configure_env "project_name" "$(basename "${project_dir}")"
configure_env "project_description" ""
configure_env "project_branch" "$(git branch | grep \\\* | cut -d ' ' -f2)"

configure_env "docker_image_tag" "$(echo -e ${project_name} | tr '[:upper:]' '[:lower:]')-$(id -un | tr '[:upper:]' '[:lower:]')"
configure_env "docker_image_app_uid" "$(id -u)"
configure_env "docker_image_app_gid" "$(id -g)"
configure_env "docker_image_app_dir" "/app"
configure_env "docker_container_tag" "$(echo -e ${project_name} | tr '[:upper:]' '[:lower:]')-$(id -un | tr '[:upper:]' '[:lower:]')-instance"

configure_env "spark_master" "local[*]"
configure_env "spark_scheduler" "FAIR"
configure_env "spark_scheduler_file" "${docker_image_app_dir}/conf/fairscheduler.xml"
configure_env "spark_jars" "$(2>&- find "${project_dir}/lib/" -type f -name "*.jar" -printf ",${docker_image_app_dir}/lib/%P" | cut -c2-)"

configure_env "jdbc_batch_size" "2000"

configure_env "jdbc_conn_sample_url"
configure_env "jdbc_conn_sample_user"
configure_env "jdbc_conn_sample_password"

##### Configure the FAIR scheduler for Spark
if [ ! -f "${project_dir}/conf/$(basename "${spark_scheduler_file}")" ] && [ -f "${project_dir}/etc/$(basename "${spark_scheduler_file}")" ]; then
  cp "${project_dir}/etc/$(basename "${spark_scheduler_file}")" "${project_dir}/conf/$(basename "${spark_scheduler_file}")"
fi
